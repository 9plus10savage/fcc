import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

let dataset;

window.addEventListener("DOMContentLoaded", async () => {
    dataset = await getDataset();
    console.log("Dataset: ", dataset)
    createBarChartFromData(dataset);
});

async function getDataset(url = "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json") {
    try {
        console.log("Fetching data...");
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error("Error fetching data: ", error);
    }
}

function createBarChartFromData(data) {
    // SVG dimensions
    const width = 900;
    const height = 460;
    const margin = { top: 20, right: 20, bottom: 20, left: 40 };
    
    // Parsing data to match JS dateformat for x-axis as time series
    const parsedData = data.data.map(d => [new Date(d[0]), d[1]]);
    
    // Calculate bar width based on number of data points
    const barWidth = (width - margin.left - margin.right) / parsedData.length;

    // Tooltip reference
    const tooltip = d3.select(".bar-chart-tooltip")

    // Create scales for chart, time series and linear
    const xScale = d3.scaleTime()
        .domain(d3.extent(parsedData, d => d[0])) // Assuming sequential, non-time-based data for simplicity
        .range([margin.left, width - margin.right]);

    const yScale = d3.scaleLinear()
        .domain([0, d3.max(parsedData, d => d[1])])
        .range([height - margin.bottom, margin.top]);

    // Create SVG element by selecting the chart div
    const svg = d3.select("#chart")
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", [0, 0, width, height])
        .attr("style", "max-width: 100%; height: auto;")

    // Create make-shift SVG background
    svg.append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", width)
        .attr("height", height)
        .attr("fill", "hsl(0, 0%, 97%)")
        .attr("fill-opacity", 0.5);
    
    // Create bars
    const bars = svg.selectAll("rect")
        .data(parsedData)
        .enter()
        .append("rect")
        .attr("x", (d) => xScale(d[0]))
        .attr("y", (d) => yScale(d[1]))
        .attr("width", barWidth)
        .attr("height", (d) => height - margin.bottom - yScale(d[1]))
        .attr("fill", "navy")
        .attr("class", "bar")
        .attr("data-date", (d) => {
            const date = d[0].toISOString().split('T')[0];
            tooltip.attr("data-date", date); // Set "data-date" on the tooltip
            return date;
        })
        .attr("data-gdp", (d) => d[1])
        .on("mouseover", (event, d) => {
            const date = d[0].toISOString().split('T')[0];
            tooltip.attr("style", "visibility: visible")
                .attr("data-date", date) // Set "data-date" on the tooltip
                .html(`Date: ${date}<br>GDP: $${d[1]} Billion`)
                .style("left", (event.pageX + 16) + "px") // Position the tooltip
                .style("top", (event.pageY - 28) + "px");
        })
        .on("mouseout", () => {
            tooltip.attr("style", "visibility: hidden")
                .attr("data-date", ""); // Optionally clear "data-date" on the tooltip
        });

    // Create axes
    svg.append("g")
        .attr("id", "x-axis")
        .attr("transform", `translate(0, ${height - margin.bottom})`)
        .call(d3.axisBottom(xScale));

    svg.append("g")
        .attr("id", "y-axis")
        .attr("transform", `translate(${margin.left}, 0)`)
        .call(d3.axisLeft(yScale));
}

