#!/bin/bash

# echo "Enter your username:"

PSQL="psql --username=freecodecamp --dbname=number_guess -t --no-align -c"

echo "Enter your username:"

read -n 22 USERNAME

USERNAME_CHECK=$($PSQL "SELECT username, games_played, best_game FROM users WHERE username = '$USERNAME'")

if [[ -z $USERNAME_CHECK ]]
then
  echo "Welcome, $USERNAME! It looks like this is your first time here."
  INSERT_USER_RESULT=$($PSQL "INSERT INTO users(username) VALUES('$USERNAME')")
else
  echo $USERNAME_CHECK | while IFS="|" read -r USERNAME_DB GAMES_PLAYED BEST_GAME
  do
    echo "Welcome back, $USERNAME_DB! You have played $GAMES_PLAYED games, and your best game took $BEST_GAME guesses."
  done
fi

RANDOM_NUM=$(( $RANDOM % 1000 + 1 ))
NUM_GUESSES=0

GUESS_NUMBER() {
  local SECRET_NUM=$1
  local GUESS=$2
  local message=$3

  ((NUM_GUESSES++))

  if [[ $GUESS =~ ^[0-9]+$ ]]
  then
    if (( $GUESS > $SECRET_NUM ))
    then
      echo "It's lower than that, guess again:"
      read USER_GUESS
      GUESS_NUMBER $SECRET_NUM $USER_GUESS
    elif (( $GUESS < $SECRET_NUM ))
    then
      echo "It's higher than that, guess again:"
      read USER_GUESS
      GUESS_NUMBER $SECRET_NUM $USER_GUESS
    else
      echo "You guessed it in $NUM_GUESSES tries. The secret number was $SECRET_NUM. Nice job!"
      
      UPDATE_GAMES_PLAYED=$($PSQL "UPDATE users SET games_played = games_played + 1 WHERE username = '$USERNAME'")
      
      BEST_GAME_CHECK=$($PSQL "SELECT best_game FROM users WHERE username = '$USERNAME'")
      if [[ -z $BEST_GAME_CHECK || $NUM_GUESSES < $BEST_GAME_CHECK ]]
      then
        UPDATE_BEST_GAME=$($PSQL "UPDATE users SET best_game = $NUM_GUESSES WHERE username = '$USERNAME'")
      fi
      exit 0
    fi
  else
    echo "That is not an integer, guess again:"
    read USER_GUESS
    GUESS_NUMBER $SECRET_NUM $USER_GUESS
  fi
}

echo "Guess the secret number between 1 and 1000:"

read USER_GUESS

GUESS_NUMBER $RANDOM_NUM $USER_GUESS