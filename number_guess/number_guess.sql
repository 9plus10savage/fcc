--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Ubuntu 12.17-1.pgdg22.04+1)
-- Dumped by pg_dump version 12.17 (Ubuntu 12.17-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE number_guess;
--
-- Name: number_guess; Type: DATABASE; Schema: -; Owner: freecodecamp
--

CREATE DATABASE number_guess WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C.UTF-8' LC_CTYPE = 'C.UTF-8';


ALTER DATABASE number_guess OWNER TO freecodecamp;

\connect number_guess

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: public; Owner: freecodecamp
--

CREATE TABLE public.users (
    username character varying(22) NOT NULL,
    games_played integer DEFAULT 0,
    best_game integer
);


ALTER TABLE public.users OWNER TO freecodecamp;

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: freecodecamp
--

INSERT INTO public.users VALUES ('apathriel', 1, 8);
INSERT INTO public.users VALUES ('user_1719435693607', 2, 339);
INSERT INTO public.users VALUES ('user_1719435693608', 5, 342);
INSERT INTO public.users VALUES ('user_1719435709182', 2, 32);
INSERT INTO public.users VALUES ('user_1719435709183', 5, 246);
INSERT INTO public.users VALUES ('user_1719435711960', 2, 370);
INSERT INTO public.users VALUES ('user_1719435711961', 5, 160);
INSERT INTO public.users VALUES ('user_1719435735778', 2, 629);
INSERT INTO public.users VALUES ('user_1719435735779', 5, 260);
INSERT INTO public.users VALUES ('apathrie', 1, 8);
INSERT INTO public.users VALUES ('user_1719435798469', 2, 12);
INSERT INTO public.users VALUES ('user_1719435798470', 5, 368);
INSERT INTO public.users VALUES ('user_1719435806876', 2, 552);
INSERT INTO public.users VALUES ('user_1719435806877', 5, 159);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: freecodecamp
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);


--
-- PostgreSQL database dump complete
--

