#! /bin/bash

if [[ $1 == "test" ]]
then
  PSQL="psql --username=postgres --dbname=worldcuptest -t --no-align -c"
else
  PSQL="psql --username=freecodecamp --dbname=worldcup -t --no-align -c"
  echo Succesfully conected to database worldcup!
fi

# Do not change code above this line. Use the PSQL variable above to query your database.
LOG_TRUNCATE=$($PSQL "TRUNCATE TABLE games, teams")

if [[ $LOG_TRUNCATE == "TRUNCATE TABLE" ]]
then
  echo "Successfully truncated tables from database!"
fi

cat games.csv | while IFS="," read YEAR ROUND WINNER OPPONENT WINNER_GOALS OPPONENT_GOALS
do
  if [[ $WINNER != winner ]]
  then
    # start by getting the id, testing if it already exists
    WINNING_TEAM_ID=$($PSQL "SELECT team_id FROM teams WHERE name='$WINNER'")
    
    # check if empty
    if [[ -z $WINNING_TEAM_ID ]]
    then
      INSERT_TEAM_RESULT=$($PSQL "INSERT INTO teams(name) VALUES('$WINNER')")
      if [[ $INSERT_TEAM_RESULT == "INSERT 0 1" ]]
      then
        echo "Inserted into teams from winners column, $WINNER"
      fi
      WINNING_TEAM_ID=$($PSQL "SELECT team_id FROM teams where name ='$WINNER'")
    fi
  fi
  # NOW WERE DOING IT FOR OPPONENT, IDK IF CAN BE OPTIMIZED, EASIEST WOULD BE FUNCTION
  if [[ $OPPONENT != opponent ]]
  then
    # start by getting the id, testing if it already exists
    OPPONENT_TEAM_ID=$($PSQL "SELECT team_id FROM teams WHERE name='$OPPONENT'")
    
    # check if empty
    if [[ -z $OPPONENT_TEAM_ID ]]
    then
      INSERT_TEAM_RESULT=$($PSQL "INSERT INTO teams(name) VALUES('$OPPONENT')")
      if [[ $INSERT_TEAM_RESULT == "INSERT 0 1" ]]
      then
        echo "Inserted into teams from winners column, $OPPONENT"
      fi
      OPPONENT_TEAM_ID=$($PSQL "SELECT team_id FROM teams where name ='$OPPONENT'")
    fi
  fi

  if [[ $YEAR != year ]] # don't have to check all of them, can just check one same row
  then
    WINNING_TEAM=$($PSQL "SELECT team_id FROM teams WHERE name='$WINNER'")
    OPPONENT_TEAM=$($PSQL "SELECT team_id FROM teams WHERE name='$OPPONENT'")
    INSERT_GAME_RESULT=$($PSQL "INSERT INTO games(year, round, winner_id, opponent_id, winner_goals, opponent_goals) VALUES('$YEAR', '$ROUND', $WINNING_TEAM, $OPPONENT_TEAM, $WINNER_GOALS, $OPPONENT_GOALS)")
  fi
done