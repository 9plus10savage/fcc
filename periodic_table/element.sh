PSQL="psql --username=freecodecamp --dbname=periodic_table -t --no-align -c"

# Handle no arguments passed
if [[ -z $1 ]]
then
  echo "Please provide an element as an argument."
fi


# Check if number, if not, check string length for symbol/name
if [ -n "$1" ]; then
  if [[ $1 =~ ^[0-9]+$ ]]; then
    SELECT_RESULT=$($PSQL "SELECT * FROM properties INNER JOIN types USING (type_id) INNER JOIN elements USING (atomic_number) WHERE atomic_number = $1")
    if [[ -z $SELECT_RESULT ]]
    then
      echo "I could not find that element in the database."
    else
    echo "$SELECT_RESULT" | while IFS="|" read -r ATOMIC_NUMBER TYPE_ID ATOMIC_MASS MELTING_POINT BOILING_POINT TYPE SYMBOL NAME
    do
      echo "The element with atomic number $ATOMIC_NUMBER is $NAME ($SYMBOL). It's a $TYPE, with a mass of $ATOMIC_MASS amu. $NAME has a melting point of $MELTING_POINT celsius and a boiling point of $BOILING_POINT celsius."
    done
    fi
  else
    if [ ${#1} -gt 2 ]; then
      SELECT_RESULT=$($PSQL "SELECT * FROM properties INNER JOIN types USING (type_id) INNER JOIN elements USING (atomic_number) WHERE name = '$1'")
          if [[ -z $SELECT_RESULT ]]
          then
            echo "I could not find that element in the database."
          else
      echo "$SELECT_RESULT" | while IFS="|" read -r ATOMIC_NUMBER TYPE_ID ATOMIC_MASS MELTING_POINT BOILING_POINT TYPE SYMBOL NAME
      do
        echo "The element with atomic number $ATOMIC_NUMBER is $NAME ($SYMBOL). It's a $TYPE, with a mass of $ATOMIC_MASS amu. $NAME has a melting point of $MELTING_POINT celsius and a boiling point of $BOILING_POINT celsius."
      done
      fi
    else
      SELECT_RESULT=$($PSQL "SELECT * FROM properties INNER JOIN types USING (type_id) INNER JOIN elements USING (atomic_number) WHERE symbol = '$1'")
          if [[ -z $SELECT_RESULT ]]
          then
            echo "I could not find that element in the database."
          else
      echo "$SELECT_RESULT" | while IFS="|" read -r ATOMIC_NUMBER TYPE_ID ATOMIC_MASS MELTING_POINT BOILING_POINT TYPE SYMBOL NAME
      do
        echo "The element with atomic number $ATOMIC_NUMBER is $NAME ($SYMBOL). It's a $TYPE, with a mass of $ATOMIC_MASS amu. $NAME has a melting point of $MELTING_POINT celsius and a boiling point of $BOILING_POINT celsius."
      done
      fi
    fi
  fi
fi
